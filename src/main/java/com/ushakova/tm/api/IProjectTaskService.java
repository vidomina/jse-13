package com.ushakova.tm.api;

import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // Task Controller
    List<Task> findAllTaskByProjectId(String projectId);

    // Task Controller
    Task bindTaskByProject(String projectId, String taskId);

    // Task Controller
    Task unbindTaskFromProject(String taskId);

    // Project Controller
    Project removeProjectById(String projectId);

}
